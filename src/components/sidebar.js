import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Dashboard from './dashboard';
import Users from './users';
import Create from "./users/create";
import Edit from "./users/edit";

function sidebar() {
    return (
        <Router>
            <div class="main-sidebar sidebar-style-2">
                <aside id="sidebar-wrapper">
                    <div class="sidebar-brand">
                        <Link to="/dashboard">Dashboard Monolith</Link>
                    </div>
                    <div class="sidebar-brand sidebar-brand-sm">
                        <Link to="/dashboard">DM</Link>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="dropdown active">
                            <ul class="dropdown-menu">
                                <li><Link class="nav-link" to="/dashboard">Website Statistics</Link></li>
                                <li><Link class="nav-link" to="/dashboard/users">List of Users</Link></li>
                                <li><Link class="nav-link"><i class="fas fa-sign-out-alt"></i> Logout</Link></li>
                            </ul>
                        </li>
                    </ul>
                </aside>
            </div>
            <Switch>
                <Route exact path="/dashboard">
                    <Dashboard />
                </Route>
                <Route exact path="/dashboard/users">
                    <Users />
                </Route>
                <Route exact path="/dashboard/users/create">
                    <Create />
                </Route>
                <Route exact path="/dashboard/users/edit">
                    <Edit />
                </Route>
            </Switch>
        </Router>
    );
}
export default sidebar;