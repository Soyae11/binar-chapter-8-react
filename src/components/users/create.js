function create() {
    return (
        <div>
            <div class="main-wrapper main-wrapper-1">
                <div class="main-content">
                    <section class="section">
                        <div class="section-header">
                            <h1>Dashboard</h1>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                <form method="post" action="/dashboard/create" class="needs-validation" novalidate="">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>Create Article</h4>
                                        </div>
                                        <div class="card-body pb-0">
                                            <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" name="title" class="form-control" required />
                                                <div class="invalid-feedback">
                                                    Please fill in the title
                                                        </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Content</label>
                                                <textarea class="summernote-simple" name="body"></textarea>
                                            </div>
                                        </div>
                                        <div class="card-footer pt-0">
                                            <button class="btn btn-primary">Create Article</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    );
}

export default create;