import {
    Link
} from "react-router-dom";
function users() {
    return (
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Dashboard</h1>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Articles List</h4>
                                <div class="card-header-action">
                                    <Link class="btn btn-primary" to="/dashboard/users/create">Create</Link>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Author</th>
                                                <th>Approved</th>
                                                <th>Views</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="/dashboard/article/">
                                                        Where is your finger?
                                                    </a>
                                                    <div class="table-links">
                                                        <div class="bullet"></div>
                                                        <a href="/article/">View</a>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="po" class="font-weight-600">
                                                        VSauce
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="badge badge-success font-weight-600">Approved</div>
                                                </td>
                                                <td>
                                                    <p href="lala" class="font-weight-600">
                                                        10
                                                    </p>
                                                </td>
                                                <td><Link to="users/edit" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i
                                                        class="fas fa-pencil-alt"></i></Link>
                                                <a href="/dashboard/article/delete/"
                                                    class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i
                                                        class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
            </section >
        </div >
    );
}

export default users;